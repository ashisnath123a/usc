## #coding: utf8
import sys
import requests
import webbrowser as wb
import xlrd
import simplejson as json
from restkit import Resource, BasicAuth, request
import os


def createTestRun(server_base_url, user, password, projectkey, testcasekey):
    #"This method is used to create testrun key for a test case which in turn used to update testresult"

    auth = BasicAuth(user, password)
    complete_url = server_base_url+"/rest/atm/1.0/testrun/" 
    resource = Resource(complete_url, filters=[auth])
    
    try:
        data = {
            
                "name": "Test Run",
                "projectKey": projectkey,
                "status": "In Progress",
                "items": [
                      {
                       "testCaseKey": testcasekey,
                       "userKey": user,
                       "executionTime": 100
                       }
                     ]
              
           }
		   
     # Here through REST API "Post" command script is generating a Test run key for the test case.
	 # json.dumps - returns string representation of JSON object
        response =resource.post(headers={'Content-Type' : 'application/json'},payload=json.dumps(data))
        
    except Exception as ex:
        print "EXCEPTION: {0}".format(ex)
        return None

    if response.status_int / 100 != 2:
        print "ERROR: status", (response.status_int)
        return None
		
    # json.loads - returns JSON object representation of string
    Test_Run = json.loads(response.body_string())
    return Test_Run
	
	
def updateTestRun(server_base_url, user, password, Testrunkey, testcasekey):
#This method is used to update testrun using test run key for a test case.
#Change the test case result to passed/failed which in turn returns test result id.

 
    auth = BasicAuth(user, password) 
    complete_url = server_base_url+"/rest/atm/1.0/testrun/"+Testrunkey+"/testcase/"+testcasekey+"/testresult/"
    resource = Resource(complete_url, filters=[auth])
	
    try:
        data = {    
               "status": "Pass",
               "comment": "The test has passed on some automation tool procedure.",
               "userKey": user,
  
               }

 # Here REST API through the command "put" is updating test result as Pass and creating a test result id    
 
        response =resource.put(headers={'Content-Type' : 'application/json'},payload=json.dumps(data))
    except Exception as ex:
        print "EXCEPTION: {0}".format(ex)
        return None

    if response.status_int / 100 != 2:
        print "ERROR: status", (response.status_int)
        return None
 
    Testresult = json.loads(response.body_string())
    return Testresult
	
	
	
def testResultAttach(server_base_url, user, password, Testresultid,filename):

#This method is used to attach log file to the test case using the test result id
 
    complete_url = server_base_url+"/rest/atm/1.0/testresult/"+str(Testresultid)+"/attachment"  
    files={'file':open(filename,'rb')}
	
    try:
        response =requests.post(complete_url,auth=(user, password), files=files)
    except Exception as ex:
        print "EXCEPTION: {0}".format(ex)
        return None

    k=response.text
    return k
	
if __name__ == '__main__':
    if (len(sys.argv) != 6):
        print "Usage: ",(sys.argv[0])," testinputfile projectkey username password filename" 
        sys.exit(1);

    server_url = 'https://ais-jira.usc.edu:8443'
	
	#Arguments which are passed
	
    testinputfile = sys.argv[1]
    projectkey = sys.argv[2]
    username =sys.argv[3]
    password = sys.argv[4] 
    filename=sys.argv[5]
	
	#Code to get list of testcases from excelsheet file exported from JIRA.
	
    workbook=xlrd.open_workbook(testinputfile)
    sh=workbook.sheet_by_name("Sheet0")
    print "Total test cases are ", sh.nrows-1
    testkeylist=[]
    i=0
       
	#Iterating through Excel sheet values and obtaining the test case key which is passed to the methods.	
	
    for n in range(1,sh.nrows):
        testcasekey =sh.cell_value(n,i)
        testrun = createTestRun(server_url, username, password, projectkey, testcasekey)
       
        testrunkey = testrun["key"]
        print "########################################################################"
        print "Executing test case {0} and uploading Log files to test result".format(n)
        print "Test run key has been generated {0}".format(testrunkey)
        testresult = updateTestRun(server_url, username, password, testrunkey, testcasekey)
        testresultid = testresult["id"]
        print "Test result id has been generated {0}".format(testresultid)
        testresultattachment = testResultAttach(server_url, username, password, testresultid, filename)
        print "Test result attachment has been completed {0}".format(testresultattachment)
    
   
   
    



