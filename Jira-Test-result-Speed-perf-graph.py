#!/usr/bin/python
## #coding: utf8
import sys
import requests
#import webbrowser as wb
#import xlrd
#import simplejson as json
#from restkit import Resource, BasicAuth, request
import os
import pandas as pd
import re
from win32com.client import Dispatch
import time

# This script needs Testcase key number to get into the test case run attachment file and from there it
# will make a speed upgradation graph in a excel file called SpeedResultGraph.xslx in the same folder where
# the script is being run. 
# 
# Modules need to be installed --
#
#pip install pypiwin32
#pip install pywin32
#pip install requests
#pip install pandas


def testResultRetrieve(server_base_url, user, password, Testcasekey):

#   Through REST API accessing the test resul attachement id number

    complete_url = server_base_url+"/rest/atm/1.0/testcase/"+str(Testcasekey)+"/testresult/latest"  
    
    	
    try:
        response =requests.get(complete_url,auth=(user, password))
    except Exception as ex:
        print "EXCEPTION: {0}".format(ex)
        return None
    #print response
    Testresultattachement = str(response.text)
    
    reg=r'\"id\":[1-9]{1,}'
    s= re.search(reg,Testresultattachement)
    t = s.group(0).split(":")[1] 
    #print t
	
   
    return t
	
	
def testResultPresentation(server_base_url, user, password, Testattachmentid):

#  Through REST API accessing the attachment file 

    complete_url = server_base_url+"/rest/tests/1.0/attachment/"+str(Testattachmentid)  
    dval=[]
    uval=[]
    try:
        response =requests.get(complete_url,auth=(user, password))
    except Exception as ex:
        print "EXCEPTION: {0}".format(ex)
        return None
    print response.content
    attachementfile = str(response.content)
    
    # Search download and upload values in pre and post migration conditions
    
    regd=r'Download: [0-9]{1,}.[0-9]{1,2}'
    regu=r'Upload: [0-9]{1,}.[0-9]{1,2}'
    
    sp= re.findall(regd,attachementfile)
         
    d = {v:k for k,v in (s.split(': ') for s in sp)}

    for k,v in d.iteritems():
        dval.append(k)
	
    #print dval
    sp1= re.findall(regu,attachementfile)
    d1 = {v1:k1 for k1,v1 in (s1.split(': ') for s1 in sp1)}

    for k1,v1 in d1.iteritems():
        uval.append(k1)
    
    dval = dval[::-1]
    
    #print uval, dval
    
    return uval,dval
	
def testResultSpeedXcelgraph(uval, dval):
    
    # Speed data to plot.
    ufval=[]
    ufval.append(float(uval[0]))
    ufval.append(float(uval[1]))
    list_data = ufval
    # Create a dataframe from the data.
    df = pd.DataFrame(list_data)
	# Speed data to plot.
    dfval=[]
    dfval.append(float(dval[0]))
    dfval.append(float(dval[1]))
    list_data1 = dfval
    # Create a dataframe from the data.
    df1 = pd.DataFrame(list_data1)
    
    # Create a Excel writer using XlsxWriter as the engine.
    sheet_name = "Upload"
    sheet_name1 = "Download"
    excel_file = 'SpeedResultGraph.xlsx'
    
    
    writer = pd.ExcelWriter(excel_file, engine='xlsxwriter')
    df.to_excel(writer, sheet_name=sheet_name)
    df1.to_excel(writer, sheet_name=sheet_name1)
    
    # Access the XlsxWriter workbook and worksheet objects from the dataframe.
    # This is equivalent to the following using XlsxWriter on its own:
    
    
    workbook = writer.book
    worksheet = writer.sheets[sheet_name]
    worksheet1 = writer.sheets[sheet_name1]
    
    
    # Create a chart object.
    chart = workbook.add_chart({'type': 'column'})
    chart1 = workbook.add_chart({'type': 'column'})
    
    # Configure the series of the chart from the dataframe data.
    chart.add_series({
        'values':     '=Upload!$B$2:$B$8',
        'gap':        1,
    })
    
    chart1.add_series({
     	'values':     '=Download!$B$2:$B$8',
        'gap':        1,
    })
    
    # Configure the chart axes.
    chart.set_y_axis({'major_gridlines': {'visible': False},'name': 'Mbits/s'})
    chart1.set_y_axis({'major_gridlines': {'visible': False},'name': 'Mbits/s'})
    # Turn off chart legend. It is on by default in Excel.
    chart.set_legend({'position': 'none'})
    chart1.set_legend({'position': 'none'})
    
    # Insert the chart into the worksheet.
    worksheet.insert_chart('F5', chart)
    worksheet1.insert_chart('F5', chart1)
    chart.set_title({'name': 'Upload Speed Difference '})
    chart1.set_title({'name': 'Download Speed Difference '})
    
    #chart.set_y_axis({'name': 'Random jiggly bit values'})
    #chart.set_x_axis({'name': 'Sequential order'})
    
    # Close the Pandas Excel writer and output the Excel file.
    writer.save()
    return excel_file

    
if __name__ == '__main__':
  
    # if (len(sys.argv) != 6):
        # print "Usage: ",(sys.argv[0])," testinputfile projectkey username password filename" 
        # sys.exit(1);
		
    server_url = 'https://ais-jira.usc.edu:8443'
	
	#Arguments which are passed
	
    testkey = sys.argv[1]
    #projectkey = sys.argv[2]
    #username =sys.argv[2]
    username = sys.argv[2]
    password = sys.argv[3]
	
    #password = sys.argv[3] 
    #filename=sys.argv[5]
    
    # Attachment id retrieving 
    Testattachmentid = testResultRetrieve(server_url, username, password, testkey)
    
    #upload and download speed value collections from pre and post upgradation states
    uval,dval = testResultPresentation(server_url, username, password, Testattachmentid)
    
    #Excel file opening 
    excelFile=testResultSpeedXcelgraph(uval, dval)
    
    print "########################################################################"
    print "Graph is created in SpeedResultGraph.xlsx file",
    print "########################################################################"
    print "Please wait for 5 seconds it will show you the graph.",
    
    time.sleep (5)
    
    #Get current working directory to read the graph excell file
    
    curDir = os.getcwd()
    
    xl = Dispatch("Excel.Application")
    xl.Visible = True # otherwise excel is hidden

 # newest excel file will open excel file and show the graph
    wbo = xl.Workbooks.Open(curDir+r'\SpeedResultGraph.xlsx')

	
    #print testrun
    